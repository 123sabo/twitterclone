This is simple twitter clone application.

Migrations will create default admin user with following credentials:

Email: admin@admin.hr

Password: admin


Setup steps:


1. Run git clone in your terminal:

    git clone git@bitbucket.org:123sabo/twitterclone.git


2. Setup TRUSTED_HOSTS and DATABASE_URL variabes in your local configuration in .env.local

    eg.

    TRUSTED_HOSTS=twitterclone.local

    DATABASE_URL=mysql://root:toor@127.0.0.1:3306/twitterclone


3. Run "composer install" in your terminal (make sure that your current working directory is twitterclone project directory)

4. Run migrations:

    php bin/console doctrine:migrations:migrate


5. Setup hostname in hosts file, for Linux users it would be something like this:

    /etc/hosts

    127.0.0.1       twitterclone.local


6. Configure your webserver to point to twitterclone/public directory. For apache users it would look something like this:

    /etc/apache2/sites-available/twitterclone.conf

    <VirtualHost *:80>
        ServerName twitterclone.local

        DocumentRoot /var/www/twitterclone/public
        <Directory /var/www/twitterclone/public>
            AllowOverride None
            Order Allow,Deny
            Allow from All

            FallbackResource /index.php
        </Directory>

        <Directory /var/www/twitterclone/public/bundles>
            FallbackResource disabled
        </Directory>
        ErrorLog /var/log/apache2/twitterclone_error.log
        CustomLog /var/log/apache2/twitterclone_access.log combined

        # optionally set the value of the environment variables used in the application
        #SetEnv APP_ENV prod
        #SetEnv APP_SECRET <app-secret-id>
        #SetEnv DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name"
    </VirtualHost>


7. Enable newly created apache configuration and restart apache service:

    sudo a2ensite twitterclone.conf

    sudo service apache2 restart


8. Open "http://twitterclone.local" in web browser 