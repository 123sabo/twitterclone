<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\PostRepository;
use App\Form\PostType;

class PostController extends AbstractController
{
    /**
     * @Route("/", defaults={"page"="1"}, methods={"GET"}, name="homepage")
     * @Route("/page/{page<[1-9]\d*>}", defaults={"page"="1"}, methods={"GET"}, name="homepagePaginated")
     */
    public function homepage(EntityManagerInterface $em, Request $request, PostRepository $postRepo)
    {   
        $page = $request->get('page', 1);
        
        if ($this->getUser()->isAdmin()) {
            $posts = $postRepo->findLatest($page);
        } else {
            $posts = $postRepo->findLatestForUser($this->getUser()->getId(), $page);
        }
        
        $error = $request->get('error');
        
        return $this->render('homepage.html.twig', ['user' => $this->getUser(), 'posts' => $posts, 'error' => $error]);
    }
    
    /**
    * @Route("/posts/create", name="postCreateGet", methods={"GET"})
    */
    public function postCreateGet()
    {
        $form = $this->createForm(PostType::class);
        
        return $this->render('post_create.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
    * @Route("/posts/create", name="postCreatePost", methods={"POST"})
    */
    public function postCreatePost(
        Request $request,
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        $form = $this->createForm(PostType::class);
        $form->handleRequest($request);
        $formData = $form->getData();
        
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($this->getUser()->getId());
        
        $post = new Post($user, $formData['post']);  
                
        $errors = $validator->validate($post);
        
        if ($errors->count() > 0) {
            return $this->render('post_create.html.twig', array(
                'form'  => $form->createView(),
                'error' => $errors->get(0)->getMessage()
            ));
        }

        $post = new Post($user, $formData['post']);
        $em->persist($post);
        $em->flush();
        
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("/posts/delete/{id}", name="postDelete", methods={"GET", "DELETE"})
     */
    public function deletePost(EntityManagerInterface $em, $id)
    {
        $postRepo = $em->getRepository(Post::class);
        
        $post = $postRepo->find($id);
        
        if (empty($post)) {
            $error = "Post does not exist";
            
            return $this->redirectToRoute('homepage', ['error' => $error]);
        }
        
        $user = $this->getUser();
        
        if (!$user->isAdmin() && ($user->getId() !== $post->getUser()->getId())) {
            $error = "Insufficient privileges";
            
            return $this->redirectToRoute('homepage', ['error' => $error]);
        }
        
        $em->remove($post);
        $em->flush();
        
        return $this->redirectToRoute('homepage');
    }
}
