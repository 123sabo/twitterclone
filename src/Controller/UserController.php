<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\UserType;

class UserController extends AbstractController
{
    /**
    * @Route("/registration", name="registrationGet", methods={"GET"})
    */
    public function registerGet()
    {
        $form = $this->createForm(UserType::class);
        
        return $this->render('registration.html.twig', array(
            'form' => $form->createView(),
            'error' => null
        ));
    }
    
    /**
    * @Route("/registration", name="registrationPost", methods={"POST"})
    */
    public function registerPost(
        Request $request,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);
        
        $userData = $form->getData();
        
        if ($form->isSubmitted() && !$form->isValid()) {          
            return $this->render('registration.html.twig', array(
                'error' => $form->getErrors(),
                'form'  => $form->createView()
                )
            );
        }
        
        $roleRepo = $entityManager->getRepository(Role::class);
        $userRole = $roleRepo->findOneBy(['role' => User::USER_TYPES_MAPPING['user']]);

        $user = new User(
            $userData['email'],
            $userData['password'],
            $userRole
        );
        
        $errors = $validator->validate($user);
        
        if ($errors->count() > 0) {
            return $this->render('registration.html.twig', array(
                'error' => $errors->get(0)->getMessage(),
                'form'  => $form->createView()
                )
            );
        }
        
        $entityManager->persist($user);
        $entityManager->flush();
        
        return $this->redirectToRoute('homepage');
    }
}
