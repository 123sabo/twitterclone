<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    const ADMIN_USER_REFERENCE = 'admin-user';

    public function load(ObjectManager $manager)
    {
        $roleAdmin = new Role('ROLE_ADMIN');
        $manager->persist($roleAdmin);
        
        $roleUser = new Role('ROLE_USER');
        $manager->persist($roleUser);

        $manager->flush();
        
        $this->addReference(self::ADMIN_USER_REFERENCE, $roleAdmin);
    }
}
