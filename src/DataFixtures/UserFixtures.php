<?php

namespace App\DataFixtures;

use App\DataFixtures\RoleFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = new User('admin@user.hr', 'admin');
        $admin->setRole($this->getReference(RoleFixtures::ADMIN_USER_REFERENCE));
        $manager->persist($admin);

        $manager->flush();
    }
    
    public function getDependencies()
    {
        return array(
            UserFixtures::class
        );
    }
}
