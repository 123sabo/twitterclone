<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="posts")
 */
class Post
{
    const NUM_ITEMS = 5;
    

    /**
     *
     * @var type integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var type string
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="email")
     */
    protected $user;
 
    /**
     *
     * @var type string
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\Length(
     *      min = 5,
     *      max = 255
     * )
     */
    protected $text;
    
    public function __construct(User $user, String $text)
    {
        $this->setUser($user);
        $this->setText($text);
    }
    
    public function setUser(User $user) : self
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function setText(string $text) : self
    {
        $this->text = $text;
        
        return $this;
    }
    
    public function getText() : string
    {
        return $this->text;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function getId() : int
    {
        return $this->id;
    }
}
    