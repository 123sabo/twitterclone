<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 */
class Role
{
    /**
     * @var type integer
     * 
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false, unique=true)
     */
    protected $id;
    
    /**
     *
     * @var type string
     * 
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    protected $role;
    
    /**
     *
     * @var type array
     */
    protected $users;
    
    public function __construct(string $role)
    {
        $this->role = $role;
    }
    
    public function getRoleName() : string
    {
        return $this->role;
    }
}