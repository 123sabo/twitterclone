<?php

namespace App\Entity;

use App\Entity\Role;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * 
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    const USER_TYPES_MAPPING = [
        "admin"     => "ROLE_ADMIN",
        "user"   => "ROLE_USER"
    ];
    
    /**
     *
     * @var type string
     * 
     * @ORM\Id
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * 
     * @Assert\Email()
     */
    protected $email;
    
    /**
     *
     * @var type string
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="users")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;
    
    /**
     *
     * @var type string
     * 
     * @ORM\Column(type="string", length=255)
     */
    protected $password;
    
    /**
     *
     * @var type string
     * 
     * @Assert\NotBlank()
     */
    protected $passwordPlain;

    /**
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    protected $posts;
    
    /**
     *
     * @var type string
     * 
     * @ORM\Column(type="string", length=20)
     */
    protected $salt;

    public function __construct(string $email, string $password, ?Role $role)
    {
        $this->setEmail($email);
        $this->passwordPlain = $password;
        $this->posts = array();
        $this->setRole($role);
        $this->salt = bin2hex(openssl_random_pseudo_bytes(5));
    }


    public function eraseCredentials() : self
    {
        $this->passwordPlain = null;
        
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
    
    public function setPassword(string $password) : self
    {
        $this->password = $password;
        
        return $this;
    }

    public function getRoles() {
        return [$this->role->getRoleName()];
    }

    public function getSalt() : string
    {
        return $this->salt;
    }

    public function getUsername(): string
    {
        return $this->email;
    }
    
    public function getEmail() : string
    {
        return $this->email;
    }
    
    public function setEmail(string $email) : self
    {
        $this->email = $email;
        
        return $this;
    }
    
    public function getPosts() : array
    {
        return $this->posts;
    }
    
    public function getId() : string
    {
        return $this->getEmail();
    }
    
    public function getPasswordPlain() : ?string
    {
        return $this->passwordPlain;
    }
    
    public function setRole(Role $role) : self
    {
        $this->role = $role;
        
        return $this;
    }
    
    public function isAdmin() : bool
    {
        if (in_array(self::USER_TYPES_MAPPING['admin'], $this->getRoles())) {
            return true;
        }
        
        return false;
    }
}