<?php

namespace App\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\User;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PersistUserSubscriber implements EventSubscriber
{
    /**
     *
     * @var type UserPasswordEncoderInterface
     */
    protected $passwordEncoder;
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist
        );
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        /**
         * @todo This is called on every entity persist, it's far from optimal
         * 
         * @var type User
         */
        $object = $args->getObject();
        
        if ($object instanceof User) {
            $object->setPassword(
                $this->passwordEncoder->encodePassword($object, $object->getPasswordPlain())
            );
        }
    }
}