<?php

namespace App\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserProvider implements UserProviderInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function loadUserByUsername($username): UserInterface {
        $userRepo = $this->entityManager->getRepository(User::class);
        
        $user = $userRepo->findOneBy(['email' => $username]);
        
        if (empty($user)) {
            throw new UsernameNotFoundException();
        }
        
        return $user;
    }

    public function refreshUser(UserInterface $user): UserInterface {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        
        $userRepo = $this->entityManager->getRepository(User::class);
        
        $userReloaded = $userRepo->findOneBy(['email' => $user->getEmail()]);
        
        if (empty($userReloaded)) {
            throw new UsernameNotFoundException();
        }
        
        return $userReloaded;
    }

    public function supportsClass($class): bool {
        return User::class === $class;
    }

}